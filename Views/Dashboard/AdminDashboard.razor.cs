﻿using FloodSensorFrontend.Classes;
using FloodSensorFrontend.Data;
using Microsoft.AspNetCore.Components;
using Radzen.Blazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Views.Dashboard
{
    public partial class AdminDashboard:ComponentBase
    {
        protected Classes.Dashboard Dashboard { get; set; }
        protected override async Task OnInitializedAsync()
        {
            await appViewModel.GetDashboard();
            Dashboard = appViewModel.Dashboard;
            if(Dashboard.DashboardId == 0)
            {
                await appViewModel.CreateTemplateDashboard();
            }

        }
        private RenderFragment AddContent(object content) => builder =>
        {
            builder.AddContent(1, content);
        };
    }
}
