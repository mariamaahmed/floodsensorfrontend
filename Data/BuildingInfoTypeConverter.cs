﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace FloodSensorFrontend.Data
{
    public class BuildingInfoTypeConverter: TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string input)
            {
                Building result = new Building();
                var chuncks = value.ToString().Split(";", StringSplitOptions.RemoveEmptyEntries);
                result.BuildingId = chuncks[0];
                result.NumberOfFloors = int.Parse(chuncks[1]);
                result.FirstFloorNumber = int.Parse(chuncks[2]);
                return result;
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
