﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace FloodSensorFrontend.Data
{
    public class TokenResult
    {
        [JsonPropertyName("jwt")]
        public string Jwt { get; set; }
    }
    public class LoginResult
    {
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("role")]
        public string Role { get; set; }

        [JsonPropertyName("iat")]
        public DateTime IssuedAt { get; set; }

        [JsonPropertyName("sub")]
        public string Sub { get; set; }
    }
}
