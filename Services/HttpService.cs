﻿using System.Net.Http;
using System;
using System.Threading.Tasks; 
using FloodSensorFrontend.Helpers;
using FloodSensorFrontend.Data;
using System.Collections.Generic;
using Microsoft.AspNetCore.Components;
using System.Text.Json;
using System.Net;
using System.Text;
using System.Net.Http.Json;
using System.Net.Http.Headers;
using System.IdentityModel.Tokens.Jwt;

namespace FloodSensorFrontend.Services
{
    public interface IHttpService
    {
        Task<TokenResult> Login(string uri, LoginModel model);
        Task<T> Get<T>(string uri, bool authorize= true);
        Task<T> Get<T>(string uri, bool authorize, params KeyValuePair<string, object>[] args);
        Task Post(string uri, object value, bool authorize= true);
        Task<T> Post<T>(string uri, object value, bool authorize = true);
        Task Put(string uri, object value, bool authorize = true, params KeyValuePair<string, object>[] args);
        Task<T> Put<T>(string uri, object value, bool authorize= true);
        Task<T> Delete<T>(string uri, bool authorize= true);
    }
    public class HttpService : IHttpService
    {
        private readonly HttpClient _httpClient;
        private NavigationManager _navigationManager;
        private ILocalStorageService _localStorageService;
        private string _tokenKey = "token";
        private string _userKey = "user";

        public HttpService(HttpClient client, NavigationManager navigationManager, ILocalStorageService localStorageService)
        {
            _httpClient = client;
            _navigationManager = navigationManager;
            _localStorageService = localStorageService;
        }

        public async Task<TokenResult> Login(string uri, LoginModel model)
        {
            // mariam's basic login 
            var request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", (model.Username + ":" + model.Password).EncodeBase64());
            return await sendRequest<TokenResult>(request, false);

            // johans login
            // return await Post<TokenResult>(uri, model, false);
        }

        public async Task<T> Get<T>(string uri, bool authorize=true)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            return await sendRequest<T>(request, authorize);
        }

        public async Task<T> Get<T>(string uri, bool authorize, params KeyValuePair<string, object>[] args)
        {
            if (args.Length > 0)
            {
                uri += "?";
                for (int i = 0; i < args.Length; i++)
                {
                    uri += args[i].Key + "=" + args[i].Value + "&";
                }
                uri = uri.Remove(uri.Length - 1);
            }
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            return await sendRequest<T>(request, authorize);
        }

        public async Task Post(string uri, object value, bool authorize = true)
        {
            var request = createRequest(HttpMethod.Post, uri, value);
            await sendRequest(request, authorize);
        }

        public async Task<T> Post<T>(string uri, object value, bool authorize = true)
        {
            var request = createRequest(HttpMethod.Post, uri, value);
            return await sendRequest<T>(request, authorize);
        }

        public async Task Put(string uri, object value, bool authorize = true, params KeyValuePair<string, object>[] args)
        {
            if (args.Length > 0)
            {
                uri += "?";
                for (int i = 0; i < args.Length; i++)
                {
                    uri += args[i].Key + "=" + args[i].Value + "&";
                }
                uri = uri.Remove(uri.Length - 1);
            }
            var request = createRequest(HttpMethod.Put, uri, value);
            await sendRequest(request, authorize);
        }

        public async Task<T> Put<T>(string uri, object value, bool authorize = true)
        {
            var request = createRequest(HttpMethod.Put, uri, value);
            return await sendRequest<T>(request, authorize);
        }
        public async Task<T> Delete<T>(string uri, bool authorize = true)
        {
            var request = createRequest(HttpMethod.Delete, uri);
            return await sendRequest<T>(request, authorize);
        }

        // helper methods

        private HttpRequestMessage createRequest(HttpMethod method, string uri, object value = null)
        {
            var request = new HttpRequestMessage(method, uri);
            if (value != null)
            {
                var content = JsonSerializer.Serialize(value);
                request.Content = new StringContent(content, Encoding.UTF8, "application/json");
            }
            return request;
        }

        private async Task sendRequest(HttpRequestMessage request, bool authorize)
        {
            if (authorize)
            {
                await addJwtHeader(request);
            }

            // send request
            HttpResponseMessage response = await _httpClient.SendAsync(request);
            await handleErrors(response, authorize);
        }

        private async Task<T> sendRequest<T>(HttpRequestMessage request, bool authorize)
        {
            if (authorize)
            {
                await addJwtHeader(request);
            }

            // send request
            HttpResponseMessage response = await _httpClient.SendAsync(request);
            try
            {

                await handleErrors(response, authorize);

                var options = new JsonSerializerOptions();
                options.PropertyNameCaseInsensitive = true;
                options.Converters.Add(new StringConverter());
                var responseObj =  await response.Content.ReadFromJsonAsync<T>(options);
                return responseObj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (response.Content != null)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseString);
                }
                throw;
            }
        }

        private async Task addJwtHeader(HttpRequestMessage request)
        {
            var token = await _localStorageService.GetItem<string>(_tokenKey);
            if (!string.IsNullOrEmpty(token))
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            else
            {
                var user = await _localStorageService.GetItem<User>(_userKey);
                if(user != null)
                    request.Headers.Authorization = new AuthenticationHeaderValue("Basic", (user.Email + ":" + user.Password).EncodeBase64());
            }
        }

        private async Task handleErrors(HttpResponseMessage response, bool isAuthorized)
        {
            // throw exception on error response
            if (!response.IsSuccessStatusCode)
            {
                if(response.Content != null)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseString);
                }
                // auto logout on 401 response
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    if (isAuthorized)
                    {
                        _navigationManager.NavigateTo("account/logout");
                        throw new Exception("UnAuthorized Content");
                    }
                    else
                    {
                        throw new Exception("Invalid Username or Password");
                    }
                }
                else
                {
                    var error = await response.Content.ReadFromJsonAsync<Dictionary<string, string>>();
                    if (error.ContainsKey("error"))
                    {
                        throw new Exception(error["error"]);
                    }
                    else
                    {
                        throw new Exception("Request failed");
                    }
                }
            }
        }

    }
}