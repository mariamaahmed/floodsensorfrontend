﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class SensorType
    {
        public string SensorTypeName { get; set; }
        public string SensorTypeId { get; set; }
        public string Icon { get; set; }

        public SensorType()
        {

        }

        public SensorType(string name, string id)
        {
            this.SensorTypeName = name;
            this.SensorTypeId = id;
        }
    }
}
