﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class RegisterModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", ErrorMessage = "Invalid Email Format")]
        public string Email { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Username should be at least 6 characters")]
        public string Username { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Password must be at least 6 characters")]
        public string Password { get; set; }


        [Required]
        public string OrganizationName { get; set; }

        [Required]
        public string OrganizationAliasName { get; set; }

        [JsonIgnore]
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public RegisterModel()
        {
        }
    }
}
