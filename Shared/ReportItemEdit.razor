﻿@using System.Reflection;
@inject Radzen.DialogService dialogService
@inject IAppViewModel appViewModel

<style>
    #Colors .rz-button{
        padding: 0 5px;
        border-radius: 25px !important;
        margin: 5px;
    }
    #Icons .rz-listbox-list {
        display: flex !important;
        flex-wrap: wrap;
        flex-direction: row;
    }

    #Icons .rz-listbox-list .rz-multiselect-item{
        padding: 0;
        margin: 5px;
        cursor: pointer;
    }
    #Icons .rz-state-highlight.rz-multiselect-item {
        border-radius: 6px;
        box-shadow: inset 0px 0px 0px 10px red;
        box-sizing: border-box; 
    }
    
    .rz-listbox{
        border: 0;
    }

    .rz-listbox .rz-multiselect-item {
        color: black;
        display:flex !important;
    }
    .rz-listbox .rz-multiselect-item span {
        flex-shrink: initial;
        width:100%;
    }

    .rz-listbox .rz-multiselect-item  div[class="col"] label{
        line-height: 2.4;
    }

    .rz-listbox-header{
        display:none;
    }
</style>


<div>
    @if (Item != null && (Report.ReportType == ReportTemplate.InfoLink || Report.ReportType == ReportTemplate.SummaryList))
    {
        <div id="Colors">
            <RadzenSelectBar @bind-Value=@Item.ColorClassName TValue="string" Data="Colors" Change="(args) => OnChange(new ChangeEventArgs(){ Value = Item})">
            </RadzenSelectBar>
        </div>

        <div id="Icons">

        <RadzenListBox @bind-Value=@Item.Icon TValue="string" Data="HelperClasses.MaterialIcons" Multiple="false"
            Change="(args) =>  OnChange(new ChangeEventArgs(){ Value = Item})">
            <Template Context="Icon">
                <div class="colored @Item.ColorClassName">
                    <div class="d-inline-flex report-body w-auto border-radius-small">
                        <div class="material-icons">@Icon</div>
                    </div>
                </div>
            </Template>
        </RadzenListBox>
        </div>
    }
   
        
    <div class="accordion sub-level" id="dataSourceParent">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#dataSourceBody" aria-expanded="true" aria-controls="dataSourceBody">
                        Data Source
                    </button>
                </h2>
                <div id="dataSourceBody" class="accordion-collapse collapse" data-bs-parent="#dataSourceParent">
                    <div class="accordion-body">
                    @if(Item != null && (Report.ReportType == ReportTemplate.InfoLink || Report.ReportType == ReportTemplate.SummaryList))
                    {
                        <RadzenRadioButtonList TValue="string" @bind-Value=@Item.QueryObject>
                            <Items>
                                @foreach (var type in ObjectTypes)
                                {
                                    <RadzenRadioButtonListItem Value="@type.GetType().Name" Text="@type.GetType().Name" />
                                }
                            </Items>
                        </RadzenRadioButtonList>
                    }
                    else{ 
                        i = 1;
                        foreach(var type in ObjectTypes)
                        {
                            var targetId = "collapse" + i;
                            var parentId = "accordionParent" + i;
                            i++;
                            <div class="accordion third-level" id="@parentId">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="@('#' + targetId)" aria-expanded="true" aria-controls="@targetId">
                                            @type.GetType().Name
                                        </button>
                                    </h2>
                                    <div id="@targetId" class="accordion-collapse collapse" data-bs-parent="@('#' + parentId)">
                                        <div class="accordion-body">
                                            <RadzenListBox TValue="IEnumerable<string>" Value="SelectedFields"
                                                Change="(args) => ObjectAdded(args,type.GetType().Name)" 
                                                ValueProperty="PropertyName" 
                                                Data="type.FilterableProperties" 
                                                Multiple="true" 
                                                TextProperty="PropertyDisplayName" 
                                                Style="width: 100%">
                                                <Template Context="Property">
                                                    <div class="w-100">
                                                        <label class="form-check-label" for="flexCheckDefault">
                                                            @Property.PropertyDisplayName
                                                        </label>
                                                    </div>
                                                </Template>
                                            </RadzenListBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            }
                        }
                </div>
            </div>
        </div>
    </div>
    
     <div class="accordion sub-level" id="conditionsParent">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#conditionsBody" aria-expanded="true" aria-controls="conditionsBody">
                        Conditions
                    </button>
                </h2>
                <div id="conditionsBody" class="accordion-collapse collapse" data-bs-parent="#conditionsParent">
                    <div class="accordion-body">
                    @{ i = 1;
                        foreach(var type in ObjectTypes)
                        {
                            var targetId = "collapse" + i;
                            var parentId = "accordionParent" + i;
                            i++;
                            <div class="accordion third-level" id="@parentId">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="headingOne">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="@('#' + targetId)" aria-expanded="true" aria-controls="@targetId">
                                            @type.GetType().Name
                                        </button>
                                    </h2>
                                    <div id="@targetId" class="accordion-collapse collapse" data-bs-parent="@('#' + parentId)">
                                        <div class="accordion-body">
                                            <RadzenListBox 
                                            Change="(args) => {ConditionAdded(args);}" 
                                            @bind-Value="Item.QueryConditions"
                                            Multiple="true" 
                                            Data="type.FilterableProperties" ValueProperty="FilterString" 
                                            TextProperty="PropertyName" Style="width: 100%">
                                            <Template Context="Property">
                                                <div class="w-100">
                                                    <div class="row row-cols-3">
                                                        <div class="col">
                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                @Property.PropertyDisplayName
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <select class="form-select" @onchange="(args) => { Property.Operator = args.Value.ToString();}">
                                                                @foreach (var item in Operators)
                                                                {
                                                                    <option value="@item">@item</option>
                                                                }
                                                            </select>
                                                        </div>
                                                        <div class="col">
                                                        @{
                                                            if(Property.PropertyType == typeof(string))
                                                            {
                                                                <input type="text" class="form-control" @onchange="(args) => { Property.FilterValue = args.Value.ToString();}">
                                                            }
                                                            else if(Property.PropertyType == typeof(int))
                                                            {
                                                                <input type="number" class="form-control" @onchange="(args) => { Property.FilterValue = args.Value.ToString();}">
                                                            }
                                                            else if(Property.PropertyType == typeof(bool))
                                                            {
                                                                <input type="checkbox" class="form-checkbox" @onchange="(args) => { Property.FilterValue = args.Value.ToString();}" />
                                                            }
                                                            else if(Property.PropertyType == typeof(DateTime))
                                                            {
                                                                <RadzenDatePicker TValue="DateTime?"/>
                                                            }
                                                            else if (Property.HasDataSource)
                                                            {
                                                                <select class="form-select" @onchange="(args) => { Property.FilterValue = args.Value.ToString();}">
                                                                        @{if(Property.DataSource != null)
                                                                        foreach(var item in Property.DataSource)
                                                                        {
                                                                            <option value="@item.GetType().GetProperty(Property.DataSourceValueProperty).GetValue(item, null)">@item.GetType().GetProperty(Property.DataSourceTextProperty).GetValue(item, null)</option>
                                                                        }
                                                                    }
                                                                </select>
                                                            }
                                                        }
                                                </div>
                                            </div>
                                        </div>
                                    </Template>
                                </RadzenListBox>
                            </div>
                        </div>
                    </div>
                </div>
                }
            }
                 </div>
            </div>
        </div>
    </div>
</div>

@code {
    [Parameter]
    public Report Report{ get; set; }
    [Parameter] 
    public EventCallback<Report> ReportChanged { get; set; }

    public ReportDataItem Item { get; set; }
    private List<string> SelectedFields;
    private List<string> Conditions;
    private string FunctionType;
    private int i;
    private async void OnChange(ChangeEventArgs e)
    {
        await ReportChanged.InvokeAsync(Report);
    }

    protected override async Task OnInitializedAsync()
    {
        await InitObjectTypes();
    }

    private async void ObjectAdded(object args, string filterableObject)
    {
        var objectProperties = (args as EnumerableQuery<string>).ToList();

        // delete
        var toDelete = new List<ReportDataItem>();
        foreach(var column in Report.Columns)
        {
            if (column.QuerySelectedField.StartsWith(filterableObject + "."))
            {
                if (!objectProperties.Contains(column.QuerySelectedField))
                    toDelete.Add(column);
            }
        }
        foreach (var item in toDelete)
            Report.Columns.Remove(item);

        // add
        foreach(string propertyName in objectProperties)
        {
            var search = Report.Columns.Where(r => r.QuerySelectedField == propertyName).FirstOrDefault();
            if(search == null)
                Report.Columns.Add(new ReportDataItem() {  ReportId= Report.ReportId, QueryObject= filterableObject, QuerySelectedField = propertyName, DataType = DataTypes.String});
        }

        Report.IsChanged = true;
        await ReportChanged.InvokeAsync(Report);
    }

    private void ConditionAdded(object condition)
    {

    }

    private string[] Colors = FilterHelper.Colors;
    private List<FilterableObject> ObjectTypes { get; set; }
    private string[] Operators = FilterHelper.FilterOperators;

    protected override async Task OnParametersSetAsync()
    {
        Item = null;
        if(Report.ReportType == ReportTemplate.InfoLink || Report.ReportType == ReportTemplate.SummaryList)
        {
            Item = Report.Columns.ElementAt(Report.CurrentActiveColumn);

            //workaround, wouldnt work any other way
            foreach (var icon in HelperClasses.MaterialIcons)
                if (Item.Icon == icon){
                    Item.Icon = icon;
                    break;
                }
            FunctionType = "Scalar";
        }
        else
        {
            FunctionType = "Select";
            Item = Report.Columns[0];
            SelectedFields = Report.Columns.Select(c => c.QuerySelectedField).ToList();
        }
        await InitObjectTypes();
    }

    private async Task InitObjectTypes()
    {
        if (ObjectTypes == null)
        {
            ObjectTypes = new List<FilterableObject>();
            ObjectTypes.Add(new Sensor());
            ObjectTypes.Add(new Gateway());
            ObjectTypes.Add(new Incident());
            foreach (var objectInstance in ObjectTypes)
            {
                foreach (var property in objectInstance.FilterableProperties)
                {
                    if (property.HasDataSource && property.DataSource == null)
                    {
                        switch (property.PropertyType.Name)
                        {
                            //case nameof(Application):
                            //    await appViewModel.GetApplications();
                            //    break;
                            case nameof(SensorType):
                                await appViewModel.GetSensorTypes();
                                property.DataSource = appViewModel.SensorTypes;
                                break;
                            case nameof(IncidentType):

                                break;
                            case nameof(GatewayType):
                                await appViewModel.GetGatewayTypes();
                                property.DataSource = appViewModel.GatewayTypes;
                                break;
                            //case nameof(Sensor):
                            //    await appViewModel.GetSensors();
                            //    property.DataSource = appViewModel.Sensors;
                            //    break;
                            default:
                                break;
                        }
                    }
                }
            }
        }        
    }
}
