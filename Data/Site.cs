﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class Site
    {
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string OrganizationId { get; set; }

        private string _SiteDescription;
        public string SiteDescription
        {
            get
            {
                if (_SiteDescription == null)
                    _SiteDescription = "";
                return _SiteDescription;
            }
            set
            {
                _SiteDescription = value;
            }
        }
    }
}
