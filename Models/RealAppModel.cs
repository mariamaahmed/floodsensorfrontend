﻿using FloodSensorFrontend.Data;
using FloodSensorFrontend.Helpers;
using FloodSensorFrontend.Services;
using FloodSensorFrontend.Shared;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using FloodSensorFrontend.Classes;

namespace FloodSensorFrontend.Models
{
    public class RealAppModel : IAppModel
    {
        #region properties
        public User CurrentUser { get; private set; }

        private HttpService _httpService;
        private DashboardService _dashboardHelper;
        private string _userKey = "user";
        private string _tokenKey = "token";
        private ILocalStorageService _localStorageService;
        private MqttJsInterop mqttInterop;

        private SensorDataModel[] _sensors;
        public SensorDataModel[] Sensors { get => _sensors; set => _sensors = value; }

        private SensorType[] _sensorTypes;
        public SensorType[] SensorTypes { get => _sensorTypes; set => _sensorTypes = value; }

        private GatewayType[] _gatewayTypes;
        public GatewayType[] GatewayTypes { get => _gatewayTypes; set => _gatewayTypes = value; }

        private GatewayDataModel[] _gateways;
        public GatewayDataModel[] Gateways { get => _gateways; set => _gateways = value; }

        private Tag[] _tags;
        public Tag[] Tags { get => _tags; set => _tags = value; }

        private Dashboard _Dashboard;
        public Dashboard Dashboard { get => _Dashboard; set => _Dashboard = value; }

        private User[] _Users;
        public User[] Users { get => _Users; set => _Users = value; }

        private User _User;
        public User User { get => _User; set => _User = value ; }

        private Sensor _Sensor;
        public Sensor Sensor { get => _Sensor; set => _Sensor = value; }

        private Organization _Organization;
        public Organization Organization { get => _Organization; set => _Organization = value; }

        private Application _Application;
        public Application Application { get => _Application; set => _Application = value; }

        private Site _Site;
        public Site Site { get => _Site; set => _Site = value; }

        private Building _Building;
        public Building Building { get => _Building; set => _Building = value; }

        private Floor _Floor;
        public Floor Floor { get => _Floor; set => _Floor = value; }

        private Zone _Zone;
        public Zone Zone { get => _Zone; set => _Zone = value; }

        private Log[] _Logs;
        public Log[] Logs { get => _Logs; set => _Logs = value; }

        private Organization[] _Organizations;
        public Organization[] Organizations { get => _Organizations; set => _Organizations = value; }

        private Site[] _Sites;
        public Site[] Sites { get => _Sites; set => _Sites = value; }

        private Application[] _Applications;
        public Application[] Applications { get => _Applications; set => _Applications = value; }

        private Building[] _Buildings;
        public Building[] Buildings { get => _Buildings; set => _Buildings = value; }

        private Floor[] _Floors;
        public Floor[] Floors { get => _Floors; set => _Floors = value; }

        private Zone[] _Zones;
        public Zone[] Zones { get => _Zones; set => _Zones = value; }
        private Gateway _Gateway;
        public Gateway Gateway { get => _Gateway; set => _Gateway = value; }
        #endregion

        public RealAppModel(HttpService httpService, DashboardService dbHelper, ILocalStorageService localStorageService, MqttJsInterop mqttInterop)
        {
            _httpService = httpService;
            _dashboardHelper = dbHelper;
            _localStorageService = localStorageService;
            this.mqttInterop = mqttInterop;
        }

        public async Task Initialize()
        {
            CurrentUser = await _localStorageService.GetItem<User>(_userKey);
        }

        #region users
        public async Task Register(RegisterModel model)
        {
            await _httpService.Post<RegisterModel>("/register", model, true);
        }

        public async Task Login(LoginModel model)
        {
            //await _httpService.Get<string>("/health", false);
            if (model.Username.Equals("admin") && model.Password.Equals("1admin2"))
            {
                CurrentUser = new User() { FirstName= "Admin", Email = model.Username, Role = "sysadm", Password = model.Password, OrganizationId = "1" };
            }
            else
            {
                TokenResult tokenResult = await _httpService.Login("/internal/login/basic", model);
                var handler = new JwtSecurityTokenHandler();
                var jwtSecurityToken = handler.ReadJwtToken(tokenResult.Jwt);
                CurrentUser = new User() { Email = jwtSecurityToken.Payload["email"].ToString(), UserId = jwtSecurityToken.Payload["uid"].ToString(), Role = jwtSecurityToken.Payload["role"].ToString(), OrganizationId= jwtSecurityToken.Payload["organizationid"].ToString() };
                await _localStorageService.SetItem(_tokenKey, tokenResult.Jwt);
            }
            await _localStorageService.SetItem(_userKey, CurrentUser);
        }

        public async Task SubscribeForAlerts(User user)
        {
            List<Building> buildings = new List<Building>();
            string _topic = "aidirections/floodsensor/alerts/" + user.OrganizationId, concats;
            switch (user.Role)
            {
                case "":
                    break;
            }
            mqttInterop.CreateClient(user.UserId);
            foreach(var building in buildings)
            {
                concats = _topic + "/" + building.SiteId + "/" + building.BuildingId +"/#";
                mqttInterop.Connect(topic: concats, qos: 1, timeout: 30);
            }
        }

        public async Task Logout()
        {
            await _localStorageService.RemoveItem(_userKey);
            await _localStorageService.RemoveItem(_tokenKey);
            CurrentUser = null;
        }

        public async Task GetUser(string UserId)
        {
            _User = await _httpService.Get<User>("/User", true, new KeyValuePair<string, object>("UserId", UserId));
        }

        public async Task GetUsers(string IdentifierName, string IdentifierValue)
        {
            _Users = await _httpService.Get<User[]>("/users/sites/admins", true, new KeyValuePair<string, object>(IdentifierName, IdentifierValue));
        }

        public async Task GetOrgAdmins()
        {
            //_Users = new User[] { new User() { FirstName = "Nada", LastName = "Naoushi", Email= "nada.naoushi@aidirections.com", Role = "orgadmin" }, new User() { FirstName = "Johan", LastName = "Eksteen", Role= "orgadmin", Email = "joeksten@aidirection.com" }, new User() { FirstName = "Mariam", LastName = "Ahmed", Role = "orgadmin", Email = "mariamahmed@gmail.com" } };
            _Users = await _httpService.Get<User[]>("/users/admin/organization");
        }

        public async Task AddEditUser(User user)
        {
            if(user.UserId == null)
            {
                if (user.IsOrgAdmin)
                    await _httpService.Post("/createOrgAdmin", user);
                else if (user.IsSiteAdmin)
                    await _httpService.Post("/users/sites/admins", user);
                else
                    await _httpService.Post("/Users", user);
            }
            else
                await _httpService.Put("/Users", user);
        }

        #endregion

        #region organizations, sites, buildings, floors, zones
        public async Task GetOrganization(string OrganizationId)
        {

        }
        public async Task GetSite(string SiteId)
        {

        }

        public async Task GetBuilding(string BuildingId)
        {

        }
        public async Task GetFloor(string FloorId)
        {

        }
        public async Task GetZone(string ZoneId)
        {

        }

        public async Task AddEditOrganization(Organization Organization)
        {
            if (Organization.OrganizationId == null)
                await _httpService.Post("/organizations", Organization);
            else
                await _httpService.Put("/organizations", Organization, true, new KeyValuePair<string, object>("OrganizationId", Organization.OrganizationId));

        }
        public async Task AddEditSite(Site Site)
        {
            if (Site.SiteId == null)
                await _httpService.Post("/sites", Site);
            else
                await _httpService.Put("/sites", Site, true, new KeyValuePair<string, object>("SiteId", Site.SiteId));
        }
        public async Task AddEditBuilding(Building Building)
        {
            if (Building.BuildingId == null)
                await _httpService.Post("/building", Building);
            else
                await _httpService.Put("/building", Building, true, new KeyValuePair<string, object>("BuildingId", Building.BuildingId));
        }
        public async Task AddEditFloor(Floor Floor)
        {
            if (Floor.FloorId == null)
                await _httpService.Post("/floor", Floor);
            else
                await _httpService.Put("/floor", Floor, true, new KeyValuePair<string, object>("FloorId", Floor.FloorId));
        }
        public async Task AddEditZone(Zone Zone)
        {
            if (Zone.ZoneId == null)
                await _httpService.Post("/zone", Zone);
            else
                await _httpService.Put("/zone", Zone, true, new KeyValuePair<string, object>("ZoneId", Zone.ZoneId));
        }
        public async Task GetOrganizations()
        {
            if (CurrentUser.IsGlobalAdmin)
            {
                var response = await _httpService.Get<OrganizationsResponse>("/organizations", true);
                _Organizations = response.Organizations;
            }
            else
            {
                var organization = await _httpService.Get<Organization>("/organizations", true);
                _Organizations = new Organization[] { organization };
            }
        }
        public async Task GetSites(string OrganizationId)
        {
            _Sites = await _httpService.Get<Site[]>("/sites", true);
            foreach(var site in _Sites) 
                site.OrganizationId = OrganizationId;
        }
        public async Task GetBuildings(string SiteId)
        {
            _Buildings = await _httpService.Get<Building[]>("/building", true, new KeyValuePair<string, object>("SiteId", SiteId));
            foreach(var building in _Buildings)
                building.SiteId = SiteId;
        }

        public async Task GetFloors(string BuildingId)
        {
            _Floors = await _httpService.Get<Floor[]>("/floor", true, new KeyValuePair<string, object>("BuildingId", BuildingId));
            foreach(var floor in _Floors)  
                floor.BuildingId = BuildingId;
        }
        public async Task GetZones(string FloorId)
        {
            _Zones = await _httpService.Get<Zone[]>("/zone", true, new KeyValuePair<string, object>("FloorId", FloorId));
            foreach (var zone in _Zones)
                zone.FloorId = FloorId;
        }
        #endregion

        #region sensors
        public async Task GetApplications(string SiteId)
        {
            _Applications = await _httpService.Get<Application[]>("", true, new KeyValuePair<string, object>("SiteId", SiteId));
        }

        public async Task GetApplication(string ApplicationId)
        {
            throw new NotImplementedException();
        }

        public async Task GetSensors(string SensorType)
        {
            //_sensors = await _httpService.Get<SensorDataModel[]>("", true, SensorType);
            _sensors = new SensorDataModel[] { new SensorDataModel() { Name = "Sensor 1", SensorTypeId = "1", BuildingId = "1", FloorId = "5", SignalStrength = 3, ZoneId = "3", DeviceStatus = false, BatteryPercent = 50 }, new SensorDataModel() { Name = "Sensor 2", SensorTypeId = "1", SignalStrength = 1, BuildingId = "4", FloorId = "3", ZoneId = "2", DeviceStatus = true, BatteryPercent = 20 } };
        }
        public async Task GetSensors(string BuildingNumber, string Floor)
        {
            _sensors = await _httpService.Get<SensorDataModel[]>("", true, new KeyValuePair<string, object>("BuildingNumber", BuildingNumber), new KeyValuePair<string,object>("Floor", Floor));
        }

        public async Task GetSensor(string SensorId)
        {
        }

        public async Task GetSensorTypes()
        {
            _sensorTypes = new SensorType[] { new SensorType("Water Sensor", "1"), new SensorType("Light Sensor", "2") };
           // _sensorTypes = await _httpService.Get<SensorType[]>("");
        }

        public async Task GetTags()
        {
            _tags = new Tag[] { };
            //_tags = await _httpService.Get<Tag[]>("");
        }

        public async Task AddEditSensor(Sensor sensor)
        {
            await _httpService.Post("/sensor", sensor);
        }

        public async Task GetLogs()
        {
            _Logs = await _httpService.Get<Log[]>("");
        }

        #endregion

        #region gateways
        public async Task GetGatewayTypes()
        {
            _gatewayTypes = new GatewayType[] { new GatewayType("type 1", "1"), new GatewayType("type 2", "2"), new GatewayType("type 3", "3") };
            // _gatewayTypes = await _httpService.Get<GatewayType[]>("");
        }

        public async Task GetGateways()
        {
            _gateways = new GatewayDataModel[] { new GatewayDataModel() { Name = "Gateway1", FloorId = "1", FloorName = "First Floor", ZoneId = "1", ZoneName = "Zone1", GatewayId = "1", GatewayTypeId = "1" } };
            //_gateways = await _httpService.Get<GatewayDataModel[]>("");
        }

        public async Task AddEditGateway(Gateway gateway)
        {
            //await _httpService.Post("", gateway);
        }

        public async Task GetGateway(string GatewayId)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region dashboard
        public async Task GetDashboard()
        {
            _Dashboard = await _dashboardHelper.Get<Dashboard>($"api/Dashboard/{CurrentUser.OrganizationId}");
            _Dashboard.StartTracking();
            foreach (var group in _Dashboard.ReportGroups)
            {
                group.StartTracking();
                foreach (var report in group.Reports)
                {
                    report.StartTracking();
                    foreach (var reportItem in report.Columns)
                        reportItem.StartTracking();
                }
            }
        }

        public async Task<bool> SaveDashboard(Dashboard dashboard)
        {
            return await _dashboardHelper.Post<Dashboard>($"api/Dashboard/SaveDashboard", dashboard);
        }

        public async Task<bool> CreateTemplateDashboard()
        {
            return await _dashboardHelper.Get<bool>($"api/Dashboard/CreateDashboardIfNotExsists/{CurrentUser.OrganizationId}");
        }

        public async Task<ReportRecord[]> GetReportRecords(Report report)
        {
            return new ReportRecord[] { };
        }

        public async Task SetUpDashboard()
        {
        }
        #endregion
    }
}