﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace FloodSensorFrontend.Data
{
    public class Organization
    {
        [JsonPropertyName("Id")]
        public string OrganizationId { get; set; }

        [Required]
        [MaxLength(64)]
        [JsonPropertyName("Name")]
        public string OrganizationName { get; set; }


        [MaxLength(64)]
        [JsonPropertyName("AliasName")]
        public string Alias { get; set; }



        [MaxLength(128)]
        [JsonPropertyName("Description")]
        public string Description { get; set; }



        [JsonPropertyName("MaxDevicesCount")]
        public int MaxDevicesCount { get; set; }


        [JsonPropertyName("MaxGatewaysCount")]
        public int MaxGatewaysCount { get; set; }
    }
}
