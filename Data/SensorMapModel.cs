﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class SensorMapModel
    {
        public Building Building { get; set; }
        public int Floor { get; set; }
    }
}
