﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;

namespace FloodSensorFrontend.Data
{
    public class Sensor: FilterableObject
    {
        public string SensorId { get; set; }
        [Required(ErrorMessage = "Sensor name is required")]
        public string Name { get; set; }

        [Required]
        public string SensorTypeId { get; set; }


        [StringLength(16, ErrorMessage = "Must be exactly 16 characters", MinimumLength =16)]
        public string Devui { get; set; }
        public string Description { get; set; }
        public string SerialNumber { get; set; }

        [Required]
        public string ApplicationId { get; set; }
        public string SiteId { get; set; }
        public string BuildingId { get; set; }
        public string FloorId { get; set; }

        [Required(ErrorMessage = "Zone is required")]
        public string ZoneId { get; set; }

        public decimal X { get; set; }

        public decimal Y { get; set; }

        //[Range(0, 4, ErrorMessage = "Invalid Input")]
        public int SignalStrength { get; set; }
        public bool DeviceStatus { get; set; }
        public int BatteryPercent { get; set; }

        public List<SensorSettingsItem> Settings { get; set; }

        public Sensor()
        {
            Settings = new List<SensorSettingsItem>();
            Settings.Add(new SensorSettingsItem() { SettingName= "Pulse", SettingType = "int", SettingValue = 10 });
            //dynamic setting = new ExpandoObject();
            //setting.SettingName = "Pulse";
            //setting.SettingType = "int";
            //setting.SettingValue = "10";
        }

        public Sensor(string name, string type, string applicationId, string buildingId, string floorId, string zoneId, int signalStrength, bool deviceStatus, int batteryPercent)
        {
            this.Name = name;
            this.SensorTypeId = type;
            this.ApplicationId = applicationId;
            this.BuildingId = buildingId;
            this.FloorId = floorId;
            this.ZoneId = zoneId;
            this.DeviceStatus = deviceStatus;
            this.SignalStrength = signalStrength;
            this.BatteryPercent = batteryPercent;
        }

        private IEnumerable<FilterableProperty> _FilterableProperties;
        public IEnumerable<FilterableProperty> FilterableProperties
        {
            get
            {
                if(_FilterableProperties == null)
                {
                    _FilterableProperties = new List<FilterableProperty>()
                    {
                        new FilterableProperty(){ PropertyName= "Sensor.Name",PropertyDisplayName = "Name", PropertyType = typeof(string)},
                        new FilterableProperty(){ PropertyName= "Sensor.Devui",PropertyDisplayName = "Devui", PropertyType = typeof(string)},
                        new FilterableProperty(){ PropertyName= "Sensor.Description",PropertyDisplayName = "Description", PropertyType = typeof(string)},
                        new FilterableProperty(){ PropertyName= "Sensor.SerialNumber",PropertyDisplayName = "SerialNumber", PropertyType = typeof(string)},
                        new FilterableProperty(){ PropertyName= "Sensor.SignalStrength",PropertyDisplayName = "SignalStrength", PropertyType = typeof(int)},
                        new FilterableProperty(){ PropertyName= "Sensor.DeviceStatus",PropertyDisplayName = "DeviceStatus", PropertyType = typeof(bool)},
                        new FilterableProperty(){ PropertyName= "Sensor.BatteryPercent",PropertyDisplayName = "BatteryPercent", PropertyType = typeof(int)},
                        new FilterableProperty(){ PropertyName= "Sensor.ApplicationId", PropertyType = typeof(Application), HasDataSource =true, DataSourceTextProperty = "ApplicationName", DataSourceValueProperty = "ApplicationId", PropertyDisplayName = "Application"},
                        new FilterableProperty(){ PropertyName= "Sensor.SensorTypeId", PropertyType = typeof(SensorType), HasDataSource =true, DataSourceTextProperty = "SensorTypeName", DataSourceValueProperty = "SensorTypeId", PropertyDisplayName = "Sensor Type"},
                        new FilterableProperty(){ PropertyName= "Sensor.ZoneId", PropertyType = typeof(Zone), HasDataSource =true, DataSourceTextProperty = "ZoneName", DataSourceValueProperty = "ZoneId", PropertyDisplayName = "Zone"},
                    };
                }
                return _FilterableProperties;
            }
        }
    }
}

public class SensorSettingsItem
{
    public string SettingName { get; set; }
    public object SettingValue { get; set; }
    public string ValueExpression { get; set; }
    public string SettingType { get; set; }
}