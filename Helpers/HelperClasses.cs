﻿using FloodSensorFrontend.Classes;

namespace FloodSensorFrontend.Helpers
{
    public class HelperClasses
    {
        public static string[] MaterialIcons = new string[] { "info", "water_drop", "opacity", "water_damage", "local_fire_department", "warning_amber", "bolt", "signal_cellular_alt", "wifi", "sensors_off", "wifi_off" };
       

    }



    public class OrientationHelper
    {
        public static int? GetMaxItems(Orientation orientation)
        {
            switch (orientation)
            {
                case Orientation.Horizontal:
                case Orientation.Vertical:
                    return 4;
                case Orientation.HorizontalOneToTwo:
                case Orientation.HorizontalTwoToOne:
                    return 3;
                default:
                    return 4;
            }
        }
        public static string getWrapperClassName(Orientation groupOrientation, int reportIndex)
        {
            switch (groupOrientation)
            {
                case Classes.Orientation.Vertical:
                    return "py-2 px-5";
                case Classes.Orientation.Horizontal:
                    return "py-2";
                case Classes.Orientation.HorizontalOneToTwo:
                    if (reportIndex == 0)
                        return "py-2 col-8";
                    return "py-2 col-4";
                case Classes.Orientation.HorizontalTwoToOne:
                    if (reportIndex == 0)
                        return "py-2 col-4";
                    return "py-2 col-8";
                default:
                    return "";
            }
        }
    }
}
