﻿using System.Collections.Generic;

namespace FloodSensorFrontend.Data
{
    public interface FilterableObject
    {
        public IEnumerable<FilterableProperty> FilterableProperties { get; }
    }
}
