﻿using Microsoft.JSInterop;

namespace FloodSensorFrontend.Data
{
    public class MqttJsInterop
    {
        private readonly IJSRuntime _jsRuntime;
        private readonly string _host;
        private readonly int _port;

        public MqttJsInterop(IJSRuntime jsRuntime, string host, int port)
        {
            _jsRuntime = jsRuntime;
            _host = host;
            _port = port;
        }

        public async void CreateClient(string clientId)
        {
            await _jsRuntime.InvokeVoidAsync(
                "mqttFunctions.createClient",
                _host, _port, clientId);
        }

        public async void Connect(string topic, int qos, int timeout)
        {
            await _jsRuntime.InvokeVoidAsync(
                "mqttFunctions.connect",
                topic, qos, timeout);
        }

        public async void Disconnect()
        {
            await _jsRuntime.InvokeVoidAsync(
                "mqttFunctions.disconnect");
        }

        public async void Publish(string topic, string payload, int qos, bool retained)
        {
            await _jsRuntime.InvokeVoidAsync(
                "mqttFunctions.publish",
                topic, payload, qos, retained);
        }
    }
}
