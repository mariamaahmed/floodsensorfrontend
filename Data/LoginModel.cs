﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class LoginModel
    {
        [Required]
        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonIgnore]
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [Required]
        [JsonPropertyName("password")]
        public string Password { get; set; }

        [JsonIgnore]
        public bool RememberMe { get; set; }
    }
}
