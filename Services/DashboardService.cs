﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Services
{
    public class DashboardService
    {

        private readonly HttpClient httpClient;
        public DashboardService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<TValue> Get<TValue>(string MethodName)
        {
            try
            {
                return await httpClient.GetFromJsonAsync<TValue>(MethodName);
            }
            catch(Exception ex)
            {
                //var response = await httpClient.GetStringAsync(MethodName);
                //Console.WriteLine(response);
                Console.WriteLine(ex.ToString());
                return default(TValue);
            }
        }

        public async Task<bool> Post<TValue>(string MethodName, TValue Value)
        {
            try
            {
                await httpClient.PostAsJsonAsync<TValue>(MethodName, Value);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public string GetBaseUrl()
        {
            return httpClient.BaseAddress.ToString();
        }
    }
}
