// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace FloodSensorFrontend.Views.SensorManagement
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Classes;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Radzen;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Radzen.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\hp\source\repos\floodsensorfrontend\Views\SensorManagement\AddSensorView.razor"
using System.Linq;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\hp\source\repos\floodsensorfrontend\Views\SensorManagement\AddSensorView.razor"
           [Authorize]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/AddSensor")]
    [Microsoft.AspNetCore.Components.RouteAttribute("/EditSensor/{*SensorId}")]
    public partial class AddSensorView : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 194 "C:\Users\hp\source\repos\floodsensorfrontend\Views\SensorManagement\AddSensorView.razor"
       
    [Parameter]
    public string SensorId { get; set; }

    private bool loading;
    private IJSObjectReference module;

    private Sensor SensorModel;
    private IEnumerable<SensorType> sensorTypes = null;
    private IEnumerable<Tag> tags = null;
    private IEnumerable<Site> sites = null;
    private IEnumerable<Application> applications;
    private IEnumerable<Building> buildings;
    private IEnumerable<Floor> floors;
    private IEnumerable<Zone> zones;
    public string backUrl;

    protected override async Task OnInitializedAsync()
    {
        if (!string.IsNullOrEmpty(SensorId))
        {
            await appViewModel.GetSensor(SensorId);
            SensorModel = appViewModel.Sensor;
        }
        else
        {
            SensorModel = new Sensor();
        }
        await appViewModel.GetSensorTypes();
        sensorTypes = appViewModel.SensorTypes;
        await appViewModel.GetTags();
        tags = appViewModel.SensorTags;
        await appViewModel.GetSites(appViewModel.AppModel.CurrentUser.OrganizationId);
        sites = appViewModel.Sites;
        backUrl = "/Sensors/" + SensorModel.SensorTypeId;
        //module = await JSRuntime.InvokeAsync<IJSObjectReference>("import", "./js/tags.js");
        //await module.InvokeVoidAsync("InitTags");
    }

    protected override Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            if (sites != null && sites.Count() > 0)
            {
                if(string.IsNullOrEmpty(SensorModel.SiteId))
                    SensorModel.SiteId = sites.First().SiteId;
                var args = new ChangeEventArgs() { Value = SensorModel.SiteId };
                OnSiteChange(args);
            }
        }
        return base.OnAfterRenderAsync(firstRender);
    }

    async void OnSiteChange(ChangeEventArgs e)
    {
        var newSite = e.Value.ToString();
        await appViewModel.GetApplications(newSite);
        applications = appViewModel.Applications;
        await appViewModel.GetBuildings(newSite);
        buildings = appViewModel.Buildings;
        if (buildings.Count() > 0)
        {
            if (string.IsNullOrEmpty(SensorModel.BuildingId))
                SensorModel.BuildingId = buildings.First().BuildingId;
            var args = new ChangeEventArgs() { Value = SensorModel.BuildingId };
            OnBuildingChange(args);
        }
    }

    async void OnBuildingChange(ChangeEventArgs e)
    {
        var newBuilding = e.Value.ToString();
        await appViewModel.GetFloors(newBuilding);
        floors = appViewModel.Floors;
        if (floors.Count() > 0)
        {
            if (string.IsNullOrEmpty(SensorModel.FloorId))
                SensorModel.FloorId = floors.First().FloorId;
            var args = new ChangeEventArgs() { Value = SensorModel.FloorId };
            OnFloorChange(args);
        }
    }

    async void OnFloorChange(ChangeEventArgs e)
    {
        var newFloor = e.Value.ToString();
        await appViewModel.GetZones(newFloor);
        zones = appViewModel.Zones;
    }

    private async void OnValidSubmit()
    {
        AlertService.Clear();

        loading = true;
        try
        {
            await appViewModel.AddEditSensor(SensorModel);
            loading = false;
            NavigationManager.NavigateTo(backUrl);
        }
        catch (Exception ex)
        {
            AlertService.Error(ex.Message, false, false);
            loading = false;
            StateHasChanged();
        }
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAppViewModel appViewModel { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAlertService AlertService { get; set; }
    }
}
#pragma warning restore 1591
