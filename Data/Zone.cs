﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class Zone
    {
        public string ZoneId { get; set; }
        public string ZoneName { get; set; }
        public string FloorId { get; set; }
        private string _ZoneDescription;
        public string ZoneDescription
        {
            get
            {
                if (_ZoneDescription == null)
                    _ZoneDescription = "";
                return _ZoneDescription;
            }
            set
            {
                _ZoneDescription = value;
            }
        }
    }
}
