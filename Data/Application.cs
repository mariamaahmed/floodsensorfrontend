﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class Application
    {
        public string ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string SiteId { get; set; }
        public string Description { get; set; }
    }
}
