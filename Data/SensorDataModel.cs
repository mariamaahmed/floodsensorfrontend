﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class SensorDataModel
    {
        public string SensorId { get; }
        public string Name { get; set; }

        public string SensorTypeId { get; set; }
        public string SensorType { get; set; }
        public string ApplicationId { get; set; }
        public string FloorId { get; set; }
        public string ZoneId { get; set; }

        public string BuildingId { get; set; }


        public string Devui { get; set; }
        public string Description { get; set; }

        public string ApplicationName { get; set; }

        public string SerialNumber { get; set; }
        public string FloorName { get; set; }
        public string ZoneName { get; set; }

        public string BuildingName { get; set; }

        public decimal X { get; set; }

        public decimal Y { get; set; }

        public int SignalStrength { get; set; }
        public bool DeviceStatus { get; set; }
        public int BatteryPercent { get; set; }

        public SensorDataModel()
        {
            Description = "";
        }
       
    }
}
