using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FloodSensorFrontend.Services;
using FloodSensorFrontend.ViewModels;
using Radzen;
using FloodSensorFrontend.Models;
using Plk.Blazor.DragDrop;
using Microsoft.AspNetCore.Authentication.Cookies;
using FloodSensorFrontend.Data;
using Microsoft.JSInterop;

namespace FloodSensorFrontend
{
    public class Program
    {
        public static async Task Main(string[] args)
        {

            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            var baseAddress = builder.Configuration.GetValue<string>("BaseUrl");
            builder.Services.AddHttpClient<HttpService>(client => client.BaseAddress = new Uri(baseAddress));
            builder.Services.AddHttpClient<DashboardService>(client => { client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress);});
            builder.Services.AddScoped<IHttpService, HttpService>();
            builder.Services.AddScoped<ILocalStorageService, LocalStorageService>();
            
            builder.Services.AddScoped<ContextMenuService>();
            builder.Services.AddBlazorDragDrop();
            builder.Services.AddScoped<DialogService>();
            builder.Services.AddScoped<IAlertService, AlertService>(); 
            builder.Services.AddScoped<ToastService>();

            var wsHost = builder.Configuration.GetValue<string>("WsHost");
            var wsPort = builder.Configuration.GetValue<int>("WsPort");
            builder.Services.AddSingleton<MqttJsInterop>(x => new MqttJsInterop(x.GetRequiredService<IJSRuntime>(), wsHost, wsPort));

            builder.Services.AddScoped<IAppModel, RealAppModel>();
            builder.Services.AddScoped<IAppViewModel, AppViewModel>();

            builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();


            var host = builder.Build();
            var accountService = host.Services.GetRequiredService<IAppModel>();
            await accountService.Initialize();

            await host.RunAsync();
        }
    }
}
