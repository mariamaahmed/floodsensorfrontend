﻿using FloodSensorFrontend.Models;
using FloodSensorFrontend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System;
using System.Net;

namespace FloodSensorFrontend.Helpers
{
    public class AppRouteView : RouteView
    {
        [Inject]
        public NavigationManager NavigationManager { get; set; }
        [Inject]
        public IAppModel Model { get; set; }

        protected override void Render(RenderTreeBuilder builder)
        {
            var authorize = Attribute.GetCustomAttribute(RouteData.PageType, typeof(AuthorizeAttribute)) != null;
            var currentUrl = new Uri(NavigationManager.Uri).PathAndQuery;
            var isLoggingIn = currentUrl.StartsWith("/login");
            if (authorize && Model.CurrentUser == null && !isLoggingIn)
            {
                if (currentUrl.Equals("/"))
                    NavigationManager.NavigateTo("/login");
                //else if (currentUrl.StartsWith("/login"))
                //    NavigationManager.NavigateTo(currentUrl);
                else
                    NavigationManager.NavigateTo($"/login?returnUrl={WebUtility.UrlEncode(currentUrl)}");
            }
            else
            {
                base.Render(builder);
            }
        }
    }
}
