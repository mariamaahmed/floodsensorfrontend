﻿using System.Text.Json.Serialization;

namespace FloodSensorFrontend.Data
{
    public class OrganizationsResponse
    {
        [JsonPropertyName("organization_count")]
        public int Count { get; set; }

        [JsonPropertyName("Organizations")]
        public Organization[] Organizations { get; set; }
    }
}
