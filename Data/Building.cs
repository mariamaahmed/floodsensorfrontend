﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    [TypeConverter(typeof(BuildingInfoTypeConverter))]
    public class Building
    {
        public string BuildingId { get; set; }
        [Required]
        public string BuildingName { get; set; }
        public string SiteId { get; set; }
        public string _BuildingDescription;

        [Required]
        public string BuildingDescription
        {
            get
            {
                if (_BuildingDescription == null)
                    _BuildingDescription = "";
                return _BuildingDescription;
            }
            set
            {
                _BuildingDescription = value;
            }
        }

        [Range(1,double.MaxValue)]
        public int NumberOfFloors { get; set; }

        [JsonIgnore]
        public int FirstFloorNumber { get; set; }
        public override string ToString()
        {
            return BuildingName + ";" + NumberOfFloors + ";" + FirstFloorNumber;
        }
    }
}
