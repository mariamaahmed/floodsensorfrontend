﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using FloodSensorFrontend.Data;
using FloodSensorFrontend.Models;
using FloodSensorFrontend.Classes;

namespace FloodSensorFrontend.ViewModels
{
    public interface IAppViewModel
    {
        IAppModel AppModel { get; }
        Dashboard Dashboard { get; }
        SensorDataModel[] Sensors { get; }
        Sensor Sensor { get; }
        Gateway Gateway { get; }
        GatewayDataModel[] Gateways { get; }

        SensorType[] SensorTypes { get; }

        GatewayType[] GatewayTypes { get; }
        Tag[] SensorTags { get; }

        User[] Users { get; }
        User User { get; }
        Organization Organization { get; }
        Application Application { get; }
        Site Site { get; }
        Building Building { get; }
        Floor Floor { get; }
        Zone Zone { get; }

        Organization[] Organizations { get; }
        Application[] Applications { get; }
        Site[] Sites { get; }
        Building[] Buildings { get; }
        Floor[] Floors { get; }
        Zone[] Zones { get; }


        Task GetSensors(string SensorType);
        Task GetSensors(string BuildingId, string FloorId);
        Task GetSensor(string SensorId);
        Task AddEditSensor(Sensor Sensor);
        Task GetOrganizations();
        Task GetSites(string OrganizationId);
        Task GetApplications(string SiteId);
        Task GetBuildings(string SiteId);
        Task GetFloors(string BuildingId);
        Task GetZones(string FloorId);
        Task GetApplication(string ApplicationId);
        Task GetOrganization(string OrganizationId);
        Task GetSite(string SiteId);
        Task GetBuilding(string BuildingId);
        Task GetFloor(string FloorId);
        Task GetZone(string ZoneId);
        Task AddEditOrganization(Organization Organization);
        Task AddEditSite(Site Site);
        Task AddEditBuilding(Building Building);
        Task AddEditFloor(Floor Floor);
        Task AddEditZone(Zone Zone);
        Task GetSensorTypes();
        Task GetGatewayTypes();
        Task GetGateways();
        Task GetGateway(string GatewayId);
        Task AddEditGateway(Gateway Gateway);
        Task GetTags();

        Task Register(RegisterModel model);
        Task Login(LoginModel model);
        Task Logout();
        Task GetUsers(string IdentifierName, string IdentifierValues);
        Task GetUser(string UserId);
        Task AddEditUser(User User);
        Task GetDashboard();
        Task<bool> SaveDashboard(Dashboard dashboard);
        Task<bool> CreateTemplateDashboard();
        Task<ReportRecord[]> GetReportValues(Report Report);
    }

    public class AppViewModel : INotifyPropertyChanged, IAppViewModel
    {

        private IAppModel _AppModel;
        public IAppModel AppModel { get => _AppModel; }

        public Dashboard Dashboard { get => _AppModel.Dashboard; }

        public SensorDataModel[] Sensors { get => _AppModel.Sensors; }
        public Sensor Sensor { get => _AppModel.Sensor; }
        public Gateway Gateway { get => _AppModel.Gateway; }
        public GatewayDataModel[] Gateways { get => _AppModel.Gateways; }

        public SensorType[] SensorTypes { get => _AppModel.SensorTypes; }

        public Tag[] SensorTags { get => _AppModel.Tags; }

        public GatewayType[] GatewayTypes { get => _AppModel.GatewayTypes; }

        public Application[] Applications { get => _AppModel.Applications; }

        public User[] Users { get => _AppModel.Users; }

        public User User { get => _AppModel.User; }
        public Organization Organization { get => _AppModel.Organization; }
        public Application Application { get => _AppModel.Application; }
        public Site Site { get => _AppModel.Site; }
        public Building Building { get => _AppModel.Building; }
        public Floor Floor { get => _AppModel.Floor; }
        public Zone Zone { get => _AppModel.Zone; }
        public Organization[] Organizations { get => _AppModel.Organizations; }

        public Site[] Sites { get => _AppModel.Sites; }

        public Building[] Buildings { get => _AppModel.Buildings; }

        public Floor[] Floors { get => _AppModel.Floors; }

        public Zone[] Zones { get => _AppModel.Zones; }

        public AppViewModel(IAppModel appModel)
        {
            _AppModel = appModel;
        }

        public async Task GetSensors(string SensorType)
        {
            await _AppModel.GetSensors(SensorType);
            NotifyPropertyChanged("Sensors");
        }

        public async Task GetSensors(string BuildingId, string FloorId)
        {
            await _AppModel.GetSensors(BuildingId, FloorId);
            NotifyPropertyChanged("Sensors");
        }

        public async Task GetOrganizations()
        {
            await _AppModel.GetOrganizations();
            NotifyPropertyChanged("Organizations");
        }

        public async Task GetSites(string OrganizationId)
        {
            await _AppModel.GetSites(OrganizationId);
            NotifyPropertyChanged("Sites");
        }

        public async Task GetApplications(string SiteId)
        {
            await _AppModel.GetApplications(SiteId);
            NotifyPropertyChanged("Applications");
        }

        public async Task GetBuildings(string SiteId)
        {
            await _AppModel.GetBuildings(SiteId);
            NotifyPropertyChanged("Buildings");
        }

        public async Task GetFloors(string BuildingId)
        {
            await _AppModel.GetFloors(BuildingId);
            NotifyPropertyChanged("Floors");
        }
        public async Task GetZones(string ZoneId)
        {
            await _AppModel.GetZones(ZoneId);
            NotifyPropertyChanged("Zones");
        }
        public async Task GetOrganization(string OrganizationId)
        {
            await _AppModel.GetOrganization(OrganizationId);
            NotifyPropertyChanged("Organization");
        }
        public async Task GetSite(string SiteId)
        {
            await _AppModel.GetSite(SiteId);
            NotifyPropertyChanged("Site");
        }

        public async Task GetBuilding(string BuildingId)
        {
            await _AppModel.GetBuilding(BuildingId);
            NotifyPropertyChanged("Building");
        }
        public async Task GetFloor(string FloorId)
        {
            await _AppModel.GetFloor(FloorId);
            NotifyPropertyChanged("Floor");
        }
        public async Task GetZone(string ZoneId)
        {
            await _AppModel.GetZone(ZoneId);
            NotifyPropertyChanged("Zone");
        }
        public async Task AddEditOrganization(Organization Organization)
        {
            await _AppModel.AddEditOrganization(Organization);
        }
        public async Task AddEditSite(Site Site)
        {
            await _AppModel.AddEditSite(Site);
        }
        public async Task AddEditBuilding(Building Building)
        {
            await _AppModel.AddEditBuilding(Building);
        }
        public async Task AddEditFloor(Floor Floor)
        {
            await _AppModel.AddEditFloor(Floor);
        }
        public async Task AddEditZone(Zone Zone)
        {
            await _AppModel.AddEditZone(Zone);
        }

        public async Task GetSensorTypes()
        {
            await _AppModel.GetSensorTypes();
            NotifyPropertyChanged("SensorTypes");
        }

        public async Task GetGatewayTypes()
        {
            await _AppModel.GetGatewayTypes();
            NotifyPropertyChanged("GatewayTypes");
        }

        public async Task GetGateways()
        {
            await _AppModel.GetGateways();
            NotifyPropertyChanged("Gateways");
        }

        public async Task GetApplication(string ApplicationId)
        {
            await _AppModel.GetApplication(ApplicationId);
            NotifyPropertyChanged("Application");
        }

        public async Task GetGateway(string GatewayId)
        {
            await _AppModel.GetGateway(GatewayId);
            NotifyPropertyChanged("Gateway");
        }

        public async Task AddEditGateway(Gateway gateway)
        {
            await _AppModel.AddEditGateway(gateway);
        }

        public async Task GetTags()
        {
            await _AppModel.GetTags();
            //NotifyPropertyChanged("SensorTags");
        }

        public async Task GetDashboard()
        {
            await _AppModel.GetDashboard();
            NotifyPropertyChanged("Dashboard");
        }

        public async Task GetUsers(string IdentifierName, string IdentifierValue)
        {
            await _AppModel.GetUsers(IdentifierName, IdentifierValue);
            NotifyPropertyChanged("Users");
        }

        public async Task Register(RegisterModel model)
        {
            await _AppModel.Register(model);
        }
        public async Task Login(LoginModel model)
        {
            await _AppModel.Login(model);
        }

        public async Task Logout()
        {
            await _AppModel.Logout();
        }

        public async Task<ReportRecord[]> GetReportValues(Report Report)
        {
            return await _AppModel.GetReportRecords(Report);
        }

        public async Task<bool> SaveDashboard(Dashboard dashboard)
        {
            return await _AppModel.SaveDashboard(dashboard);
        }

        public async Task<bool> CreateTemplateDashboard()
        {
            return await _AppModel.CreateTemplateDashboard();
        }

        public async Task GetSensor(string SensorId)
        {
            await _AppModel.GetSensor(SensorId);
            NotifyPropertyChanged("Sensor");
        }

        public async Task GetUser(string UserId)
        {
            await _AppModel.GetUser(UserId);
            NotifyPropertyChanged("User");
        }

        public async Task AddEditSensor(Sensor Sensor)
        {
            await AppModel.AddEditSensor(Sensor);
        }

        public async Task AddEditUser(User User)
        {
            await AppModel.AddEditUser(User);
        }

        #region INotifyPropertyChanged implementations
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected void SetValue<T>(ref T backingFiled, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingFiled, value)) return;
            backingFiled = value;
            NotifyPropertyChanged(propertyName);
        }
        #endregion

    }
}