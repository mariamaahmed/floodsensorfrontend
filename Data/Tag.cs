﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class Tag
    {
        public string TagName { get; set; }
        public int TagId { get; set; }

        public Tag()
        {

        }

        public Tag(string name, int id)
        {
            this.TagName = name;
            this.TagId = id;
        }
    }
}
