#pragma checksum "C:\Users\hp\source\repos\floodsensorfrontend\Shared\SummaryList.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c5e79001b1ba7e90cd986e8628f4b987451467ed"
// <auto-generated/>
#pragma warning disable 1591
namespace FloodSensorFrontend.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using FloodSensorFrontend.Classes;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Radzen;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Users\hp\source\repos\floodsensorfrontend\_Imports.razor"
using Radzen.Blazor;

#line default
#line hidden
#nullable disable
    public partial class SummaryList : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Radzen.Blazor.RadzenCard>(0);
            __builder.AddAttribute(1, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenElement(2, "div");
                __builder2.AddAttribute(3, "class", "d-flex");
#nullable restore
#line 5 "C:\Users\hp\source\repos\floodsensorfrontend\Shared\SummaryList.razor"
          
            for (int i = 0; i < Report.Columns.Count; i++)
            {
                var Item = Report.Columns[i];

#line default
#line hidden
#nullable disable
                __builder2.OpenElement(4, "div");
                __builder2.AddAttribute(5, "class", "colored" + " " + (
#nullable restore
#line 9 "C:\Users\hp\source\repos\floodsensorfrontend\Shared\SummaryList.razor"
                                     Item.ColorClassName

#line default
#line hidden
#nullable disable
                ));
                __builder2.OpenElement(6, "div");
                __builder2.AddAttribute(7, "class", "report-header my-2");
#nullable restore
#line 10 "C:\Users\hp\source\repos\floodsensorfrontend\Shared\SummaryList.razor"
__builder2.AddContent(8, Item.Header);

#line default
#line hidden
#nullable disable
                __builder2.CloseElement();
                __builder2.AddMarkupContent(9, "\r\n                    ");
                __builder2.OpenElement(10, "div");
                __builder2.AddAttribute(11, "class", "d-inline-flex report-body border-radius-big");
                __builder2.AddMarkupContent(12, "<div style=\"width:100%; min-width:100px\">TestValue</div>\r\n                        ");
                __builder2.OpenElement(13, "span");
                __builder2.AddAttribute(14, "class", "material-icons");
#nullable restore
#line 13 "C:\Users\hp\source\repos\floodsensorfrontend\Shared\SummaryList.razor"
__builder2.AddContent(15, Item.Icon);

#line default
#line hidden
#nullable disable
                __builder2.CloseElement();
                __builder2.CloseElement();
                __builder2.CloseElement();
#nullable restore
#line 16 "C:\Users\hp\source\repos\floodsensorfrontend\Shared\SummaryList.razor"
            }
    

#line default
#line hidden
#nullable disable
                __builder2.CloseElement();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 21 "C:\Users\hp\source\repos\floodsensorfrontend\Shared\SummaryList.razor"
       

    [Parameter]
    public Report Report { get; set; }

    //private ReportItemValue DataSource { get; set; }

    protected override async Task OnInitializedAsync()
    {
        var result = await appViewModel.GetReportValues(Report);
        //if(result != null && result.Count() > 0)
        //{
        //    DataSource = result[0].Values[0];
        //}
        //else
        //{
        //    DataSource = new ReportItemValue() { AssociatedDataItem = Report.Columns[0], Name ="TestName", Value ="TestValue" };
        //}
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAppViewModel appViewModel { get; set; }
    }
}
#pragma warning restore 1591
