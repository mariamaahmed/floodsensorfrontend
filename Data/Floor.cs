﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class Floor
    {
        public string FloorId { get; set; }
        public string FloorName { get; set; }
        public string BuildingId { get; set; }
        //public string FloorDescription { get; set; }

        private string _FloorDescription;
        public string FloorDescription
        {
            get
            {
                if (_FloorDescription == null)
                    _FloorDescription = "";
                return _FloorDescription;
            }
            set
            {
                _FloorDescription = value;
            }
        }

        public Floor()
        {
            FloorDescription = "";
        }
    }
}
