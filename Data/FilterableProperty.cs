﻿using FloodSensorFrontend.Classes;
using System;

namespace FloodSensorFrontend.Data
{
    public class FilterableProperty
    {
        public string PropertyName { get; set; }

        private string _PropertyDisplayName;
        public string PropertyDisplayName { get => string.IsNullOrEmpty(_PropertyDisplayName)? PropertyName: _PropertyDisplayName; set => _PropertyDisplayName = value; }
        public Type PropertyType { get; set; }
        public string Operator { get; set; }
        public string FilterValue { get; set; }

        public bool HasDataSource { get; set; }

        public dynamic DataSource { get; set; }

        public string DataSourceTextProperty { get; set; }
        public string DataSourceValueProperty { get; set; }

        public string FilterString
        {
            get
            {
                return PropertyName + Operator + FilterValue;
            }
            set
            {
                foreach(var op in FilterHelper.FilterOperators)
                {

                }
            }
        }
    }
}
