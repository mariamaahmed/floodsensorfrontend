﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class Log
    {
        public DateTime DateTime { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
    }
}
