﻿using FloodSensorFrontend.Classes;
using FloodSensorFrontend.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class Incident: FilterableObject
    {
        public IncidentType Type { get; set; }
        public DateTime DateTime { get; set; }

        public long SensorId { get; set; }

        private IEnumerable<FilterableProperty> _FilterableProperties;
        public IEnumerable<FilterableProperty> FilterableProperties
        {
            get
            {
                if (_FilterableProperties == null)
                {
                    _FilterableProperties = new List<FilterableProperty>()
                    {
                        new FilterableProperty(){ PropertyName = "Incident.Type", PropertyType = typeof(IncidentType), HasDataSource =true, DataSourceTextProperty = "Key", DataSourceValueProperty = "Value", PropertyDisplayName = "Incident Type"},
                        new FilterableProperty(){ PropertyName = "Incident.DateTime", PropertyType = typeof(DateTime), PropertyDisplayName = "Incident Date Time"},
                        new FilterableProperty(){ PropertyName = "Incident.SensorId", PropertyType = typeof(Sensor), HasDataSource =true, DataSourceTextProperty = "Name", DataSourceValueProperty = "SensorId", PropertyDisplayName= "Sensor"}
                    };
                }
                return _FilterableProperties;
            }
        }
    }
}
