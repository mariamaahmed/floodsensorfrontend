﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FloodSensorFrontend.Data
{
    public class Gateway : FilterableObject
    {
        public string GatewayId { get; set; }
        [Required(ErrorMessage = "Gateway name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Gateway type is required")]
        public string GatewayTypeId { get; set; }

        public string Description { get; set; }

        //public string SerialNumber { get; set; }
        public string SiteId { get; set; }
        public string BuildingId { get; set; }
        public string FloorId { get; set; }

        [Required(ErrorMessage = "Zone is required")]
        public string ZoneId { get; set; }
        public bool DeviceStatus { get; set; }

        public Gateway()
        {

        }

        public Gateway(string name, string type, string zone, bool deviceStatus)
        {
            this.Name = name;
            this.GatewayTypeId = type;
            this.ZoneId = zone;
            this.DeviceStatus = deviceStatus;
        }

        private IEnumerable<FilterableProperty> _FilterableProperties;
        public IEnumerable<FilterableProperty> FilterableProperties
        {
            get
            {
                if (_FilterableProperties == null)
                {
                    _FilterableProperties = new List<FilterableProperty>()
                    {
                        new FilterableProperty() { PropertyName = "Gateway.Name", PropertyDisplayName = "Gateway Name", PropertyType = typeof(string) },
                        new FilterableProperty() { PropertyName = "Gateway.GatewayTypeId", PropertyType = typeof(string), HasDataSource = true, DataSourceTextProperty = "GatewayTypeName", DataSourceValueProperty = "GatewayTypeId", PropertyDisplayName ="Gateway Type" }
                    };
                }
                return _FilterableProperties;
            }
        }
    }
}