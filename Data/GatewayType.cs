﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class GatewayType
    {
        public string GatewayTypeName { get; set; }
        public string GatewayTypeId { get; set; }

        public GatewayType()
        {

        }

        public GatewayType(string name, string id)
        {
            this.GatewayTypeName = name;
            this.GatewayTypeId = id;
        }
    }
}
