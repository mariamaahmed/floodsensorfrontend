﻿using System.Threading.Tasks;
using FloodSensorFrontend.Data;
using FloodSensorFrontend.Classes;

namespace FloodSensorFrontend.Models
{
    public interface IAppModel
    {
        User CurrentUser { get; }
        Task Initialize();
        Task Register(RegisterModel model);
        Task Login(LoginModel model);
        Task Logout();
        Task GetUsers(string IdentifierName, string IdentifierValue);
        Task GetOrgAdmins();
        Task GetUser(string UserId);
        Task AddEditUser(User user);
        Task SubscribeForAlerts(User user);

        Task GetSensor(string SensorId);
        Task AddEditSensor(Sensor sensor);
        Task GetSensors(string SensorType);
        Task GetSensors(string BuildingNumber, string Floor);
        Task GetSensorTypes();
        Task GetOrganizations();
        Task GetOrganization(string OrganizationId);
        Task GetSite(string SiteId);
        Task GetBuilding(string BuildingId);
        Task GetFloor(string FloorId);
        Task GetZone(string ZoneId);
        Task GetSites(string OrganizationId);
        Task GetApplications(string SiteId);
        Task GetApplication(string ApplicationId);
        Task GetBuildings(string SiteId);
        Task GetFloors(string BuildingId);
        Task GetZones(string FloorId);
        Task AddEditOrganization(Organization Organization);
        Task AddEditSite(Site Site);
        Task AddEditBuilding(Building Building);
        Task AddEditFloor(Floor Floor);
        Task AddEditZone(Zone Zone);
        Task GetTags();

        Task AddEditGateway(Gateway gateway);
        Task GetGateways();
        Task GetGateway(string GatewayId);
        Task GetGatewayTypes();

        Task GetLogs();

        Task SetUpDashboard();
        Task GetDashboard();

        Task<bool> SaveDashboard(Dashboard dashboard);
        Task<bool> CreateTemplateDashboard();

        Task<ReportRecord[]> GetReportRecords(Report report);


        public SensorDataModel[] Sensors { get; set; }
        public Sensor Sensor { get; set; }
        public Gateway Gateway { get; set; }
        public Organization Organization { get; set; }
        public Application Application { get; set; }
        public Site Site { get; set; }
        public Building Building { get; set; }
        public Floor Floor { get; set; }
        public Zone Zone { get; set; }
        public Tag[] Tags { get; set; }
        public SensorType[] SensorTypes { get; set; }
        public Application[] Applications { get; set; }
        public Organization[] Organizations { get; set; }
        public Site[] Sites { get; set; }
        public Building[] Buildings { get; set; }
        public Floor[] Floors { get; set; }
        public Zone[] Zones { get; set; }
        public GatewayDataModel[] Gateways { get; set; }
        public GatewayType[] GatewayTypes { get; set; }



        public Dashboard Dashboard { get; set; }
        public User[] Users { get; set; }
        public User User { get; set; }
        public Log[] Logs { get; set; }
    }

}
