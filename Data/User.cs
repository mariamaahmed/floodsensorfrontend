﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class User
    {
        public string UserId { get; set; }

        public string OrganizationId { get; set; }

        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        public string Username { get; set; }
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [MinLength(6, ErrorMessage = "Minimum length is 6 characters")]
        public string Password { get; set; }
        public string SiteId { get; set; }
        public string BuildingId { get; set; }

        [JsonIgnore]
        [Required]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [JsonIgnore]
        public bool IsGlobalAdmin { get; set; }
        public bool IsOrgAdmin { get; set; }

        public bool IsSiteAdmin { get; set; }
        public bool IsDeviceAdmin { get; set; }
        public bool IsGatewayAdmin { get; set; }

        [JsonIgnore]
        public string Role
        {
            get
            {
                if (IsGlobalAdmin)
                    return "Global Admin";
                else if (IsOrgAdmin)
                    return "Organization Admin";
                else if (IsSiteAdmin)
                    return "Site Admin";
                else if (IsDeviceAdmin)
                    return "Device Admin";
                else
                    return "Gateway Admin";
            }
            set
            {
                if (value == "sysadm")
                    IsGlobalAdmin = true;
                else if (value == "orgadmin")
                    IsOrgAdmin = true;
                else if (value == "siteadmin")
                    IsSiteAdmin = true;
                else if (value == "deviceadmin")
                    IsDeviceAdmin = true;
                else if (value == "gatewayadmin")
                    IsGatewayAdmin = true;
            }
        }
    }
}
