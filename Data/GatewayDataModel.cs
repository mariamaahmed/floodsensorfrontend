﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FloodSensorFrontend.Data
{
    public class GatewayDataModel
    {
        public string GatewayId { get; set; }
        public string Name { get; set; }
        public string GatewayTypeId { get; set; }
        public string GatewayTypeName { get; set; }

        public string Description { get; set; }
        public string SiteId { get; set; }
        public string BuildingId { get; set; }

        public string FloorId { get; set; }
        public string FloorName { get; set; }
        public string ZoneId { get; set; }
        public string ZoneName { get; set; }
        public bool DeviceStatus { get; set; }

        public GatewayDataModel()
        {
            GatewayTypeName = "";
            Description = "";
        }
    }
}
